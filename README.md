# HelloWorld with BobBuildTool and Rust-Cargo

This is a Hello World project using Bob Build Tool as package manager to build a Rust-Cargo program which uses an external C/C++ library.
The Rust program is build with Cargo but the C/C++ ecosystem knows many build systems.

## Build

### Release

```sh
bob build -c release --destination dist helloworld
```

### Debug

```sh
bob dev --destination dist helloworld
```

## Execute

```sh
env LD_LIBRARY_PATH=./dist/lib ./dist/bin/helloworld
```
